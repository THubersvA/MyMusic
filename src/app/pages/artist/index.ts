import { ArtistListComponent } from './artist-list/artist-list.component';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';

export const components: any[] = [
    ArtistListComponent,
    ArtistDetailComponent
];

export * from './artist-list/artist-list.component';
export * from './artist-detail/artist-detail.component';
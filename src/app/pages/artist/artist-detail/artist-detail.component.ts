import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Artist } from '../artist.model';
import { ArtistService } from '../artist.service';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.css']
})
export class ArtistDetailComponent implements OnInit {
  artist$: Observable<Artist>;

  constructor(
    private artistService: ArtistService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.artist$ = this.route.paramMap.pipe(
      tap((params: ParamMap) => console.log('user.id = ', params.get('id'))),
      switchMap((params: ParamMap) =>
        this.artistService.getById(parseInt(params.get('id'), 10))
      ),
      tap(console.log)
    );
  }

}

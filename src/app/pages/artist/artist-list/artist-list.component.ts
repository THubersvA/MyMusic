import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Artist } from '../artist.model';
import { ArtistService } from '../artist.service';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css']
})
export class ArtistListComponent implements OnInit {
  artists$: Observable<Artist[]>;

  constructor(private artistService: ArtistService) { }

  ngOnInit(): void {
    console.log('ArtistList geladen');
    this.artists$ = this.artistService.getAll();
  }

}

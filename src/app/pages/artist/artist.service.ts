import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { Song } from '../song/song.model';
import { Artist } from './artist.model';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {
  private artist: Artist[] = [
    {
      id: 1,
      name: "Test",
      artistType: [],
      songs: [new Song],
      albums: [],
      likes: 5,
    }
  ]

  constructor() { }

  getAll(): Observable<Artist[]> {
    return of(this.artist).pipe();
  }

  getById(id: number): Observable<Artist> {
    return from(this.artist).pipe(
      filter((item) => item.id === id),
      take(1)
    );
  }

}

import { Album } from '../album/album.model';
import { Song } from '../song/song.model';

export class Artist {
  id: number;
  name: string;
  artistType: ArtistTypes[];
  songs: Song[];
  albums: Album[];
  likes: number;
}

export enum ArtistTypes {
  Singer = "Singer",
  SongWriter = "Song writer",
}
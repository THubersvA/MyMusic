import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';
  readonly ARTIST_USER = 'Artiest';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Like',
      description: 'Hiermee kan een gebruiker een song liken',
      scenario: [
        'Gebruiker klikt op like knop.',
        'Applicatie bekijkt account.',
        'Applicatie voegt song toe aan gebruikers liked lijst.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Een song is geliked.',
    },
    {
      id: 'UC-03',
      name: 'Song toevoegen',
      description: 'Hiermee kan een artiest een song toevoegen',
      scenario: [
        'Artiest klikt op "add song".',
        'Artiest vult song gegevens in.',
        'Applicatie bekijkt account en gegevens.',
        'Applicatie creëert song.'],
      actor: this.ARTIST_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Een song is toegevoegd',
    },
    {
      id: 'UC-04',
      name: 'Album toevoegen',
      description: 'Hiermee kan een artiest een album toevoegen',
      scenario: [
        'Artiest klikt op "add album".',
        'Artiest vult album gegevens in.',
        'Artiest voegt eigen nummers toe',
        'Applicatie bekijkt account en gegevens.',
        'Applicatie creëert album met bijbehorende artiesten en songs.'],
      actor: this.ARTIST_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Een album is toegevoegd',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
